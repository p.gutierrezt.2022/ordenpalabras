#!/usr/bin/env pythoh3

'''
Program to order a list of words given as arguments
'''

import sys

def is_lower(first: str, second: str):
    """Return True if first is lower (alphabetically) than second

    Order is checked after lowercasing the letters
    `<` is only used on single characteres.
    """
    A= first.lower()
    B= second.lower()

    if A < B:
         return True
    else:
        return False

def get_lower(words: list, pos: int):
    """Get lower word, for words right of pos (including pos)"""
    for x in range(pos, len(words)):
        if not is_lower(words[pos], words[x]):
            pos = x
    return pos

def sort(words: list):
    """Return the list of words, ordered alphabetically"""
    Lista = ['']
    for y in range (0,len (words)):
        numbers=get_lower(words,y)
        Lista+= [words[numbers]]
        words[numbers]=words[y]
    return Lista

def show(words: list):
    """Show words on screen, using print()"""
    return print(words)

def main():
    words: list = sys.argv[1:]
    ordered: list = sort(words)
    show(ordered)


if __name__ == '__main__':
    main()
